//Get Our Elements
const player = document.querySelector('.player');
const video = player.querySelector('.viewer');
const progress = player.querySelector('.progress');
const progressBar = player.querySelector('.progress__filled');

const toggle = player.querySelector('.toggle');
const skipButtons = player.querySelectorAll('[data-skip]');
const ranges = player.querySelectorAll('.player__slider');

const volumeSlider = player.querySelector('input[name="volume"]');
const speedSlider = player.querySelector('input[name="playbackRate"]');
const fullscreenBtn = player.querySelector('.fullscreen');

//Build Our Functions
function playToggle (e) {
  // if (video.paused) { video.play(); } else { video.pause(); }
  const method = video.paused ? 'play' : 'pause';
  video[method]();
}

function updateBtn() {
  toggle.textContent = video.paused ? '►' : '❚ ❚';
}

function handleProgress(e) {
  const amountPlayed = Math.round((video.currentTime / video.duration) * 100);
  progressBar.style.flexBasis = `${amountPlayed}%`;
}

function adjustRange(e) {
  if (isMousedown) video[e.target.name] = this.value;
}

function skipTime(e) {
  const skipAmount = e.target.dataset.skip;
  const newCurrentTime = video.currentTime + Number(skipAmount)
  video.currentTime = (newCurrentTime < 0) ? 0 : newCurrentTime;
}

function scrub(e) {
  const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
  video.currentTime = scrubTime;
}

function fullScr(e) {

  video.webkitRequestFullScreen();
}

/* Hook up the event listeners */
//Playback Toggle
video.addEventListener('play', updateBtn);
video.addEventListener('pause', updateBtn);
video.addEventListener('click', playToggle);
toggle.addEventListener('click', playToggle);

//Adjust Speed or Volume
let isMousedown = false;
ranges.forEach( range => {
  range.addEventListener('mousedown', (e) => isMousedown = true );
  range.addEventListener('mouseup', (e) => isMousedown = false );
  range.addEventListener('mousemove', adjustRange);
  range.addEventListener('change', adjustRange);
});

//Skip buttons
skipButtons.forEach( btn => {
  btn.addEventListener('click', skipTime);
});

//Progress
video.addEventListener('timeupdate', handleProgress);

//Srub
let scrubMousedown = false;
progress.addEventListener('click', scrub);
progress.addEventListener('mousemove', (e) => scrubMousedown && scrub(e));
progress.addEventListener('mousedown', () => scrubMousedown = true );
progress.addEventListener('mouseup', () => scrubMousedown = false );

//Fullscreen
fullscreenBtn.addEventListener('click', fullScr);